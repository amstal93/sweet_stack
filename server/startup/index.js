import 'dotenv/config';
import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import http from 'http';
import { 
    ApolloServer 
} from 'apollo-server-express';
import schema from '../api/schema';
import resolvers from '../api/resolvers';
import { connectDb } from '../models';

const app = express();
app.use(cors());
app.use(morgan('dev'));

const server = new ApolloServer({
    introspection: true,
    typeDefs: schema,
    resolvers,
    //context: TODO: Add current user/authToken to context
})

server.applyMiddleware({ app, path: '/graphql'});
const httpServer = http.createServer(app);
server.installSubscriptionHandlers(httpServer);

const port = process.env.PORT || 8000;
connectDb().then(async () => {
    httpServer.listen({ port }, () => 
        console.log(`Apollo Server on http://localhost:${port}/graphql`)
    );
})
