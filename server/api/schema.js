import linkSchema from './index.graphql';
import userSchema from './User/user.graphql';

export default [linkSchema, userSchema];